package com.ekissaria.demo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EkissariaHelloController {

    @GetMapping
    public String helloFromEkissariaApi() {
        return "Hello you are in Ekissaria API";
    }
}
